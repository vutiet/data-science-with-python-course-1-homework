def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6)) #15 < x + y = 16 < 20, then return 20
print(sum(10, 2)) # x + y = 12 < 15, then return 12
print(sum(10, 12)) # x + y = 22 > 20, then return 22
